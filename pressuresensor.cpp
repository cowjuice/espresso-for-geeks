/* Copyright (c) 2017 Philippe Kalaf, MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* Analog output pressure sensor driver */
#include "pressuresensor.h"

PressureSensor::PressureSensor(PinName pin) : _input(pin) {        // create the AnalogIn
}

// returns pressure in bar
float PressureSensor::read_bars()
{
    return float(read_pa())/100000;
}

// returns pressure in Pa, 100000 Pa = 1 bar
// avoids float calculations for 
int PressureSensor::read_pa()
{
    // sensor provides linear voltage between 0.5V to 5V representing 0 to 20 bars
    // 0.5V to 3.3V <-> 0 bar to 12.44 bar
    // read_u16 should return a value between 9930 and 65535 representing 0.5 to 3.3V
    // measurements show a range between 10300 and 60000 respresenting 0.5 to 3.3V
    // each 3995 represents 100000 Pa (1 bar) -> each unit represents 25 Pa
    // measurements show the 0.5V value at around 10300
    int pressure_pascal = (_input.read_u16() - 10300);
    pressure_pascal = (pressure_pascal * 25);

  return pressure_pascal;
} 

void PressureSensor::worker()
{
    if(_read_count != -1)
    {
	_read_count++;
	// keep calculating average
	_average_pressure = ( _average_pressure * (_read_count-1) 
				+ read_pa() )
			    /_read_count;
    }
}

void PressureSensor::start_count()
{
    _read_count = 0;
    _average_pressure = 0;
    _ticker.attach(callback(this, &PressureSensor::worker), 0.1);
}

float PressureSensor::stop_count()
{
    _read_count = -1;
    _ticker.detach();
    return float(_average_pressure) / 100000;
}
