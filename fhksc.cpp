/* Copyright (c) 2017 Philippe Kalaf, MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* Digmesa FHKSC flowmeter driver */
#include "fhksc.h"

FHKSC::FHKSC(PinName pin) : _interrupt(pin) {        // create the InterruptIn on the pin specified to FHKSC
        _interrupt.mode(PullDown); // an internal pull up is used, otherwise disable here
        _interrupt.rise(callback(this, &FHKSC::increment)); // attach increment function of this counter instance  
        _flow_rate_timer.start(); 
        _time_last_increment_us = 0;
}

void FHKSC::increment()
{
    int time_us = _flow_rate_timer.read_us();
    // Every 1925 pulses is 1L
    // Each pulse is 1000000/1925 = 520 ul or 0.52 ml
    _flow_ul += 520;
    _interval_between_increments_us = time_us - _time_last_increment_us;
    _time_last_increment_us = time_us;
}

// return in ml
float FHKSC::read()
{
    return float(_flow_ul)/1000;
}

uint32_t FHKSC::read_ul()
{
    return _flow_ul;
}

void FHKSC::reset_count()
{
    _flow_ul = 0;
    // reset timer since it can only run for 30 mins
    _flow_rate_timer.reset();
    _time_last_increment_us = 0;
}

// flow rate in ml per second
float FHKSC::get_flow_rate()
{
    int time_us = _flow_rate_timer.read_us();
    // if we got no incremenet in 2 seconds, flowrate is 0
    if (time_us - _time_last_increment_us > 2000000)
	return 0;

    if (_interval_between_increments_us)
	return float(0.52 / (float(_interval_between_increments_us)/1000000));
    else
	return 0;
} 
